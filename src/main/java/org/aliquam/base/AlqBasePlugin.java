/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;


public abstract class AlqBasePlugin extends AlqBasePluginAliases
{
	public static boolean debug = false;
	public static long serverThread;
	public static AlqBasePlugin baseinstance;
	public AlqBaseCommandMgr commandMgr;
	
	protected abstract void init() throws Exception;
	
	public final void onEnable()
	{
		
		try
		{
			initBase();
			init();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// Uncatched exception means that some plugin failed to initialize.
			// Aliquam is tightly coupled and we require all plugins to work properly
			// Avoid critical errors by disabling server.
			Bukkit.shutdown();
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(commandMgr != null)
			return commandMgr.onCommand(sender, command, args);
		return true;
	}
	
	@SuppressWarnings("deprecation")
    public static OfflinePlayer getOfflinePlayer(String playername)
	{
		return Bukkit.getOfflinePlayer(playername);
	}
}
