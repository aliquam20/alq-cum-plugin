package org.aliquam.base;

// Case Insensitive String
public class CaseInsensitiveString
{
	private final String originalString;
	private final String lowercasedString;
	
	
	public CaseInsensitiveString(String string)
	{
		this.originalString = string;
		if(originalString == null)
			lowercasedString  = null;
		else
			lowercasedString  = string.toLowerCase();
	}
	
	
	@Override
	public int hashCode()
	{
		if(isNull())
			return 0;
		return lowercasedString.hashCode();
	}
	
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null)
			return isNull();
		return lowercasedString.equals(other.toString().toLowerCase());
	}
	
	
	public boolean isNull()
	{
		return originalString == null;
	}
	
	
	@Deprecated
	@Override
	public String toString()
	{
		return originalString;
	}
	
	
	public String toOriginalString()
	{
		return originalString;
	}
	
	
	public String getOriginalString()
	{
		return originalString;
	}
	
	
	public static CaseInsensitiveString convert(Object o)
	{
		if(o == null)
			return new CaseInsensitiveString(null);
		return new CaseInsensitiveString(o.toString());
	}
	
}
