package org.aliquam.base;

public class LogExit
{
	private final StringBuilder out = new StringBuilder();
	
	private final String callerFileName;
	private final int callerLineNumber;
	
	public LogExit()
	{
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		StackTraceElement caller = stack[2];
		this.callerFileName = caller.getFileName();
		this.callerLineNumber = caller.getLineNumber();
		out.append("<< ").append(caller.getClassName()).append(".").append(caller.getMethodName()).append("(");
	}
	
	public LogExit arg(String name, Object value)
	{
		out.append(name).append(" = ").append(value);
		return this;
	}
	
	@Override
	public String toString()
	{
		out.append(")");
		out.append(" (").append(callerFileName).append(":").append(callerLineNumber).append(")");
		return out.toString();
	}
}
