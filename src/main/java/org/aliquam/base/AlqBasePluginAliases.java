/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;


import mkremins.fanciful.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


public abstract class AlqBasePluginAliases extends JavaPlugin
{
	public static Server server;
	public String pdf;
	
	protected void initBase()
	{
		server = getServer();
	}
	
	public static Player getPlayer(String playername)
	{
		return server.getPlayer(playername);
	}
	
	
	public static Player getPlayerExact(String playername)
	{
		return server.getPlayerExact(playername);
	}
	
	
	public static boolean dispatchConsoleCommand(String command)
	{
		return dispatchCommand(server.getConsoleSender(), command);
	}
	
	
	public static boolean dispatchCommand(CommandSender sender, String command)
	{
		server.dispatchCommand(sender, command);
		return true;
	}
	
	
	public void registerEvents(Listener eventListener)
	{
		server.getPluginManager().registerEvents(eventListener, this);
	}
	
	
	public int scheduleSyncDelayedTask(Runnable runnable)
	{
		return server.getScheduler().scheduleSyncDelayedTask(this, runnable);
	}
	
	
	public int scheduleSyncDelayedTask(Runnable runnable, long delay)
	{
		return server.getScheduler().scheduleSyncDelayedTask(this, runnable, delay);
	}
	
	
	public int scheduleSyncRepeatingTask(Runnable runnable, long delay, long repeat)
	{
		return server.getScheduler().scheduleSyncRepeatingTask(this, runnable, delay, repeat);
	}
	
	private String prepareMsg(Object msgObject, boolean pdf)
	{
		if(msgObject == null)
			msgObject = "null";
		String msg = (pdf ? getPdf() + msgObject : msgObject.toString()); 
		return ChatColor.translateAlternateColorCodes('&', msg.toString());
	}
	
	public boolean broadcast(String msg)
	{ return broadcast((Object)msg, true); }
	public boolean broadcast(String msg, boolean pdf)
	{ return broadcast((Object)msg, pdf); }
	
	public boolean broadcast(Object msg)
	{ return broadcast(msg, true); }
	public boolean broadcast(Object msg, boolean pdf)
	{
		server.broadcastMessage(prepareMsg(msg, pdf));
		return true;
	}
	
	public boolean sendMessage(Player target, String msg)           { return sendMessage(target, (Object)msg, true); }
	public boolean sendMessage(String target, String msg)           { return sendMessage(target, (Object)msg, true); }
	public boolean sendMessage(CommandSender sender, String msg)    { return sendMessage(sender, (Object)msg, true); }
	public boolean sendMessage(String target, String msg, boolean pdf) { return sendMessage(target, (Object)msg, pdf); }
	public boolean sendMessage(Player target, String msg, boolean pdf) { return sendMessage((CommandSender)target, (Object)msg, pdf); }
	public boolean sendMessage(CommandSender sender, String msg, boolean pdf) { return sendMessage(sender, (Object)msg, pdf); }
	
	public boolean sendMessage(Player target, Object msg)			{ return sendMessage(target, msg, true); }
	public boolean sendMessage(String target, Object msg)			{ return sendMessage(target, msg, true); }
	public boolean sendMessage(CommandSender sender, Object msg)	{ return sendMessage(sender, msg, true); }
	public boolean sendMessage(String target, Object msg, boolean pdf)
	{
		CommandSender sender = target.equalsIgnoreCase("CONSOLE") ? server.getConsoleSender() : getPlayerExact(target);
		return sendMessage(sender, msg, pdf);
	}
	public boolean sendMessage(Player target, Object msg, boolean pdf)
	{ return sendMessage((CommandSender)target, msg, pdf); }
	public boolean sendMessage(CommandSender sender, Object msg, boolean pdf)
	{
		if (sender != null)
			sender.sendMessage(prepareMsg(msg, pdf));
		return true;
	}
	
	public boolean sendFancyMessage(String target, FancyMessage msg) 
	{
		CommandSender sender = target.equalsIgnoreCase("CONSOLE") ? null : getPlayerExact(target);
		if (sender != null) return sendFancyMessage(sender, msg);
		else return false;
	}
	public boolean sendFancyMessage(Player target, FancyMessage msg) 
	{ return sendFancyMessage((CommandSender)target, msg); }
	public boolean sendFancyMessage(CommandSender target, FancyMessage msg) 
	{
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + target.getName() + " " + msg.toJSONString());
		return true;
	}
	
	public boolean log(String msg)
	{ return log((Object)msg, true); }
	public boolean log(String msg, boolean pdf)
	{ return log((Object)msg, pdf); }
	
	public boolean log(Object msg)
	{ return log(msg, true); }
	public boolean log(Object msg, boolean pdf)
	{
		sendMessage(server.getConsoleSender(), msg, pdf);
		return true;
	}
	
	public String getPdf()
	{
		if(pdf == null)
			pdf = "&f[&9" + getDescription().getName() + "&f]&7 "; 
		return pdf;
	}
}
