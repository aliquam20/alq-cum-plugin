package org.aliquam.base;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Deprecated
public class Zip
{
	private final static int KB = 1024;
    private final static int BUFFER_SIZE = 8*KB;
    
	public static void unzipDir(String zipFile) throws ZipException, IOException 
	{
	    File file = new File(zipFile);

	    try(ZipFile zip = new ZipFile(file))
	    {
		    String newPath = zipFile.substring(0, zipFile.length() - 4);
		    new File(newPath).mkdir();
		    Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();
		    
		    while (zipFileEntries.hasMoreElements())
		    {
		        ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
		        String currentEntry = entry.getName();
		        File destFile = new File(newPath, currentEntry);
		        //destFile = new File(newPath, destFile.getName());
		        File destinationParent = destFile.getParentFile();
	
		        // create the parent directory structure if needed
		        destinationParent.mkdirs();
	
		        if (!entry.isDirectory())
		        {
		        	try(BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry)))
		        	{
			            int currentByte;
			            byte data[] = new byte[BUFFER_SIZE]; // establish buffer for writing file
			            
			            FileOutputStream fos = new FileOutputStream(destFile);
			            try(BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE))
			            {
				            while ((currentByte = is.read(data, 0, BUFFER_SIZE)) != -1)
				                dest.write(data, 0, currentByte);
				            dest.flush();
			            }
		        	}
		        }
		        
		        /*if (currentEntry.endsWith(".zip"))
		        	unzipDir(destFile.getAbsolutePath());*/
		    }
	    }
	}
	
	public static void zipDir(String dirName, String nameZipFile) throws IOException
	{
		FileOutputStream fW = new FileOutputStream(nameZipFile);
		ZipOutputStream zip = new ZipOutputStream(fW);
		addFolderToZip("", dirName, zip);
		zip.close();
		fW.close();
	}
	
	
	private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws IOException
	{
		File folder = new File(srcFolder);
		if(folder.list().length == 0)
			addFileToZip(path, srcFolder, zip, true);
		else
		{
			for(String fileName : folder.list())
				if(path.equals(""))
					addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip, false);
				else
					addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip, false);
		}
	}
	
	
	private static void addFileToZip(String path, String srcFile, ZipOutputStream zip, boolean flag) throws IOException
	{
		File folder = new File(srcFile);
		if(flag)
			zip.putNextEntry(new ZipEntry(path + "/" + folder.getName() + "/"));
		else
		{
			if(folder.isDirectory())
				addFolderToZip(path, srcFile, zip);
			else
			{
				byte[] buf = new byte[BUFFER_SIZE];
				int len;
				FileInputStream in = new FileInputStream(srcFile);
				zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
				while((len = in.read(buf)) > 0)
					zip.write(buf, 0, len);
				in.close();
			}
		}
	}
}