package org.aliquam.base;

import org.aliquam.base.utils.AlqBaseUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class AlqBaseCommand
{
	protected AlqBasePlugin plugin;
	protected CommandSender sender;
	protected Player        senderPlayer;
	protected String        senderName;
	protected Command       command;
	protected String[]      args;
	
	@Deprecated
	protected boolean  prepare() { return true; }
	
	@Deprecated
	/**
	 * @Deprecated
	 * Use executeNew instead
	 * @return
	 */
	protected boolean  execute() 
	{
		return true; 
	}
	
	protected abstract boolean  onlyPlayer();
	protected abstract int      minArgs();
	protected abstract String[] neededPermissions(); // sender need any one of these permissions to run command
	
	protected boolean executeNew()
	{
		plugin.log("&cOld (deprecated) execute() method has been called for command " + command.getName() + "!");
		execute();
		return true;
	}
	
	public void baseInit(AlqBasePlugin plugin, CommandSender sender, Command command, String[] args)
	{
		this.plugin       = plugin;
		this.sender       = sender;
		this.senderName   = sender.getName();
		this.senderPlayer = sender instanceof Player ? (Player)sender : null;
		this.command      = command;
		this.args         = args;
	}
	
	
	public boolean run()
	{
		long commandStart = System.nanoTime();
		
		if(onlyPlayer())
			if(senderPlayer == null)
				return plugin.sendMessage(sender, "&cThis command is not available in console!");
		
		if(!senderHasPermissions())
			return plugin.sendMessage(sender, "&cYou don't have access to this command!");
		
		if(args.length < minArgs())
			return !plugin.sendMessage(sender, "&cYou have to provide more arguments!");
		
		boolean executeResult = true;
		if(prepare())
			executeResult = executeNew();
		
		long commandEnd = System.nanoTime();
		double commandElapsed = (commandEnd - commandStart);
		AlqBaseBukkitPlugin.instance.log("Completed \"/" + command.getName() + "\" in " + (commandElapsed / 1000000) + "ms."); 
		return executeResult;
	}
	
	
	
	protected <T> T getArg(int index, Class<T> type)
	{
		return getArg(index, type, null);
	}
	protected <T> T getArg(int index, Class<T> type, T defaultValue)
	{
		if(args.length <= index)
			return defaultValue;
		return AlqBaseUtils.convert(args[index], type, defaultValue);
	}
	
	
	protected String[] permissions(String... args)
	{
		return args;
	}
	
	
	private boolean senderHasPermissions()
	{
		return hasPermissions(sender, neededPermissions());
	}
	
	
	/**
	 * Returns whether player has permission to any of provided permissions
	 */
	protected boolean hasPermissions(CommandSender player, String... permissions)
	{
		if(permissions == null)
			return true;
		
		for(String permission : permissions)
			if(player.hasPermission(permission))
				return true;
		return false;
	}
}