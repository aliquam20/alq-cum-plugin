/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

//@SuppressWarnings("deprecation") // Once we delete generic cmd mgr, these two classes will be merged
public abstract class AlqBaseCommandMgr implements AlqBaseGenericCommandMgr
{
	private final AlqBasePlugin plugin;
	protected abstract AlqBaseCommand getCommand(CommandSender sender, Command command, String[] args);
	
	
	public AlqBaseCommandMgr(AlqBasePlugin plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String[] args)
	{
		AlqBaseCommand ccommand = getCommand(sender, command, args);
		return onCommand(ccommand, sender, command, args);
	}
	
	public boolean onCommand(AlqBaseCommand ccommand, CommandSender sender, Command command, String[] args)
	{
		if(ccommand != null)
		{
			ccommand.baseInit(plugin, sender, command, args);
			return ccommand.run();
		}
		return false;
	}
}