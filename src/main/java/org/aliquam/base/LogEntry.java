package org.aliquam.base;

public class LogEntry
{
	private final StringBuilder out = new StringBuilder();
	
	private boolean first = true;
	private final String callerFileName;
	private final int callerLineNumber;
	
	public LogEntry()
	{
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		StackTraceElement caller = stack[2];
		this.callerFileName = caller.getFileName();
		this.callerLineNumber = caller.getLineNumber();
		out.append(">> ").append(caller.getClassName()).append(".").append(caller.getMethodName()).append("(");
	}
	
	public LogEntry arg(String name, Object value)
	{
		if(first)
			first = false;
		else
			out.append(", ");
		
		out.append(name).append(" = ").append(value);
		return this;
	}
	
	@Override
	public String toString()
	{
		out.append(")");
		out.append(" (").append(callerFileName).append(":").append(callerLineNumber).append(")");
		return out.toString();
	}
}
