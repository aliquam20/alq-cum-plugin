package org.aliquam.base;

/**
 * See Runnable documentation
 * This one is same, just with added ability to throw Exception.
 */
@FunctionalInterface
public interface RunnableEx {
    public abstract void run() throws Exception;
}
