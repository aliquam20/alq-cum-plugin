package org.aliquam.base;

import java.util.HashSet;

// Case Insensitive Set
public class CaseInsensitiveSet
{
	private final HashSet<CaseInsensitiveString> backend = new HashSet<>();
	
	public boolean add(String value)
	{
		return backend.add(new CaseInsensitiveString(value));
	}
	
	public boolean remove(String key)
	{
		return backend.remove(new CaseInsensitiveString(key.toString()));
	}
	
	public boolean contains(String key)
	{
		return backend.contains(CaseInsensitiveString.convert(key));
	}
	
	public int size()
	{
		return backend.size();
	}
	
	@Override
	public String toString()
	{
		return backend.toString();
	}
}
