/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

//@Deprecated
public interface AlqBaseGenericCommandMgr
{
	public boolean onCommand(CommandSender sender, Command command, String[] args);
}