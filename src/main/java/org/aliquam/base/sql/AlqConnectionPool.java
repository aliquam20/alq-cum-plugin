package org.aliquam.base.sql;

import org.aliquam.base.AlqBaseBukkitPlugin;
import org.apache.commons.dbcp2.BasicDataSource;

import javax.naming.LimitExceededException;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class AlqConnectionPool
{
	private static BasicDataSource ds;

	public AlqConnectionPool(String host, String port, String database, String user, String pass) throws IOException, SQLException, PropertyVetoException
	{
		if(ds != null)
			throw new SQLException("AlqConnectionPool: Cannot create new pool, because it would hide previously created one");
		
		ds = new BasicDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUsername(user);
		ds.setPassword(pass);
		ds.setUrl("jdbc:mysql://" + host + ":" + port + "/" + database);
		
		ds.setInitialSize(4);
		ds.setMinIdle(4); // We want at least 4 connections to be ready to use
		ds.setMaxIdle(16); // But no more than 16. We don't even have that many plugins using SQL lol
		// Set to infinite in order not to run out of connections because someone forget to close them.
		// We keep track for number of opened connections in getConnection method and send error messages to console if we reach too much
		ds.setMaxTotal(-1);
	}
	
	public static synchronized Connection getConnection() throws SQLException
	{
		int active = ds.getNumActive();
		if(active > 16) // Some arbitrary choosen value. Whatever. Adjust if needed
		{
			try
			{
				AlqBaseBukkitPlugin.baseinstance.log("SQL connection pool reports that some plugin is probably forgetting about returning borrowed connections. Number of active connections: " + active);
				throw new LimitExceededException(); // Quickest method to obtain stacktrace lol
			}
			catch(LimitExceededException e)
			{
				e.printStackTrace(); // Just a trace to make it easier to trace which plugins borrow lots of connections
			}
		}
		
		Connection conn = ds.getConnection();
		// Make sure that the connections are auto commit by default. 
		// There might be some code that doesn't check this so better be safe.
		// Calling code will eventually enter transaction mode manually.
		// Calling code shouldn't make any assumptions what returned connection's autoCommit setting is though...
		conn.setAutoCommit(true);
		return conn;
	}
}
