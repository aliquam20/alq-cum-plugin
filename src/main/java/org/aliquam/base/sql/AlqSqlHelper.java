package org.aliquam.base.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AlqSqlHelper
{
	@Deprecated
	public static void tryClose(Connection connection)
	{
		try
		{
			if(connection != null)
				connection.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Deprecated
	public static void tryClose(Statement statement)
	{
		try
		{
			if(statement != null)
				statement.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	@Deprecated
	public static void tryClose(PreparedStatement statement)
	{
		try
		{
			if(statement != null)
				statement.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	@Deprecated
	public static void tryClose(ResultSet result)
	{
		try
		{
			if(result != null)
				result.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public static void closeSilently(Connection connection)
	{
		try
		{
			if(connection != null)
				connection.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public static void closeSilently(Statement statement)
	{
		try
		{
			if(statement != null)
				statement.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public static void closeSilently(PreparedStatement statement)
	{
		try
		{
			if(statement != null)
				statement.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public static void closeSilently(ResultSet result)
	{
		try
		{
			if(result != null)
				result.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
