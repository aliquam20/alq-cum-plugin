import org.aliquam.AlqUtils

plugins {
    `maven-publish`
    java
    id("org.sonarqube") version "3.0"
    jacoco
    id("io.freefair.lombok") version "6.0.0-m2"
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
}

repositories {
    mavenCentral()
    maven("https://papermc.io/repo/repository/maven-public/")
}

val alq = AlqUtils(project).withStandardProjectSetup()

val artifactId = "alq-cum-plugin"
val baseVersion = "0.0.1"
group = "org.aliquam"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    compileOnly("org.aliquam.ext.server:paper-api:1.17.1-397")

    implementation("org.aliquam.ext.libs:LibFanciful:0.4.0-SNAPSHOT")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("org.apache.commons:commons-dbcp2:2.9.0")

}

sonarqube {
    if (isJenkins) {
        properties {
            property("sonar.projectKey", "aliquam20_${artifactId}")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName!!)
            property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
        }
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
    }
    dependsOn(tasks.test) // tests are required to run before generating the report
}


tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(arrayOf(
        // Tell MapStruct that we're using plain Java
        "-Amapstruct.defaultComponentModel=default",
        // I like constructors more than (default) field setters
        "-Amapstruct.defaultInjectionStrategy=constructor",
        // MapStruct will force us to explicitly exclude unmapped fields
        "-Amapstruct.unmappedTargetPolicy=ERROR"
    ))
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    from("plugin.yml")
}

publishing {
    publications {
        create<MavenPublication>("myPublication") {
            groupId = "$group"
            artifactId = artifactId
            from(components["java"])
        }
    }
}
