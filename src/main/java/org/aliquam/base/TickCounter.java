package org.aliquam.base;

import java.util.concurrent.atomic.AtomicInteger;

public class TickCounter implements Runnable {

	private static AtomicInteger currentTick = new AtomicInteger(0);
	
	@Override
	public void run() {
		currentTick.incrementAndGet();
		//int currentTick = this.currentTick.incrementAndGet();
		//AlqBaseBukkitPlugin.instance.log("current tick: " + currentTick);
	}
	
	public static int getCurrentTick()
	{
		return currentTick.get();
	}
}
