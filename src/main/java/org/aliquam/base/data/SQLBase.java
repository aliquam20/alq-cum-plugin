/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base.data;

import org.aliquam.base.AlqBasePlugin;

@Deprecated
public class SQLBase extends SQLStatements
{	
	public SQLBase(AlqBasePlugin plugin)
	{
		super(plugin, true);
	}
	
	public SQLBase(AlqBasePlugin plugin, boolean loadFromCPlugin)	
	{
		super(plugin, loadFromCPlugin);
	}
}
