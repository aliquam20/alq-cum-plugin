package org.aliquam.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

// Case Insensitive Map
public class CaseInsensitiveMap<V>
{
	private final HashMap<CaseInsensitiveString, V> backend = new HashMap<>();
    
	public void put(String key, V value)
	{
		backend.put(new CaseInsensitiveString(key), value);
	}
	
	public V get(String key)
	{
		return backend.get(CaseInsensitiveString.convert(key));
	}
	
	public boolean containsKey(String key)
	{
		return backend.containsKey(CaseInsensitiveString.convert(key));
	}
	
	public V remove(String key)
	{
		return backend.remove(CaseInsensitiveString.convert(key));
	}
	
	public Set<String> keySet()
	{
		Set<String> keys = new HashSet<>();
		for(CaseInsensitiveString key : backend.keySet())
			keys.add(key.getOriginalString());
		return keys;
	}
	
	public Collection<V> values()
	{
		return backend.values();
	}
	
	public int size()
	{
		return backend.size();
	}
	
	public void clear()
	{
		backend.clear();
	}
	
	@Override
	public String toString()
	{
		return backend.toString();
	}
}
