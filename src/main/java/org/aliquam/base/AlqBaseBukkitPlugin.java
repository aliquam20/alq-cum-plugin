/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;

import org.aliquam.base.sql.AlqConnectionPool;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;


public class AlqBaseBukkitPlugin extends AlqBasePlugin
{
	public static AlqBaseBukkitPlugin instance;
	
	@Override
	protected void init()
	{
		scheduleSyncRepeatingTask(new TickCounter(), 1, 1);
	}
	
	@Override
	public void onLoad()
	{
		baseinstance = this;
		instance = this;
		AlqBasePlugin.serverThread = Thread.currentThread().getId();
		
		saveDefaultConfig();
		
		FileConfiguration con = getConfig();
		try
		{
			new AlqConnectionPool(
				con.getString("mysql.hostname"),
				con.getString("mysql.port"),
				con.getString("mysql.database"),
				con.getString("mysql.user"),
				con.getString("mysql.pass")
			);
		}
		catch (IOException | SQLException | PropertyVetoException e)
		{
			e.printStackTrace();
			// No SQL will result in plugin misbehaviour.
			// Avoid critical errors by disabling server.
			Bukkit.shutdown();
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(!sender.hasPermission("aliquam.admin"))
		{
			sendMessage(sender, "You are not permitted to do that!");
			return true;
		}
		debug = !debug;
		sendMessage(sender, "Debug is now " + (debug ? "enabled." : "disabled."));
		return true;
	}
}