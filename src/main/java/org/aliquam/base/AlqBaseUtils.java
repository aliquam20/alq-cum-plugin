/* cPlugin
 * Copyright (C) 2013 Norbert Kawinski (norbert.kawinski@gmail.com)

 */

package org.aliquam.base;

import org.apache.commons.lang3.time.StopWatch;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;


@Deprecated // Copied from parent package
public class AlqBaseUtils
{
	public static float lerp(double from, double to, double value)
	{
		return lerp((float)from, (float)to, (float)value);
	}
	
	public static float lerp(float from, float to, float value)
	{
		return (from * (1.0f - value)) + (to * value);
	}
	
	public static String formatToTwoDecimalPlaces(double value)
	{
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format(value);
	}
	
	public static void callSynchronouslyOnMainThread(JavaPlugin plugin, RunnableEx callable, String actionName) throws Exception
	{
		callSynchronouslyOnMainThread(plugin, () ->
		{
			callable.run();
			return null;
		}, actionName);
	}

	private static void waitForNextTick() {
		// Wait for next Bukkit tick. This one is kinda tricky, because we had an issue without this code:
		// Assume we've got world regenerate task which is taking ~50ms per tick for about 200 ticks (~10 seconds)
		// It's working great, until we run two such tasks simultaneously. The server freezes.
		// Why?
		// 1) Scheduler does 1st thread task
		// 2) Scheduler does 2nd thread task
		//   2a) Some time during this, 1st thread adds next task to the queue
		// 3) Scheduler does 1st thread task
		//   3a) Some time during this, 2nd thread adds next task to the queue
		// 4) Instead of starting next full server tick, Goto 2) because we still have some tasks in the queue...
		// And it loops 2) and 3) until both threads are done (2*~10 seconds = ~20 seconds)
		// If we wait for next server tick before adding task to the queue, we can remove this effect (at the cost of one tick delay for each task)
		// Note: We can't schedule sync delayed task, because they don't return value. This method allow return values via Future<T>
		int oldTick = TickCounter.getCurrentTick();
		int tick;
		do {
			tick = TickCounter.getCurrentTick();
		}
		while(tick == oldTick);
	}
	
	public static <T> T callSynchronouslyOnMainThread(JavaPlugin plugin, Callable<T> callable, String actionName) throws Exception
	{
		StopWatch s = new StopWatch();
		s.start();
		
		T ret;
		if(AlqBaseBukkitPlugin.serverThread == Thread.currentThread().getId())
		{
			ret = callable.call(); 
		}
		else
		{
//			RetryPolicy retryPolicy = new RetryPolicy()
//				.abortWhen(false)
//				.retryWhen(true)
//				//.retryIf(isNextTick -> (boolean)isNextTick == true)
//				.withMaxRetries(500)
//                .withDelay(10, TimeUnit.MILLISECONDS);
//			boolean isSameTick = Failsafe.with(retryPolicy).get(() -> {
//				int newCurrentTick = TickCounter.getCurrentTick();
//				//AlqBaseBukkitPlugin.instance.log("failsafe tick. " + currentTick + " == " + newCurrentTick);
//				return currentTick == newCurrentTick;
//			});
//			if(isSameTick)
//				AlqBaseBukkitPlugin.instance.log(ChatColor.RED + "Failed waiting for next tick!");
			//else
				//AlqBaseBukkitPlugin.instance.log(ChatColor.GREEN + "Success waiting for next tick!");
			waitForNextTick();
			Future<T> future = Bukkit.getScheduler().callSyncMethod(plugin, callable);
			ret = future.get();
		}
		
		double elapsedMs = ((double)s.getNanoTime()) / 1000000.d;
		if(elapsedMs > 100 || AlqBasePlugin.debug)
			AlqBasePlugin.baseinstance.log("DEBUG [Sync] " + formatToTwoDecimalPlaces(elapsedMs) + "ms " + actionName, false);
		return ret;
	}
	
	public static void runOnThread(Runnable runnable, String actionName)
	{
		Thread t = new Thread(() ->
		{
			if(AlqBasePlugin.debug)
			{
				StopWatch s = new StopWatch();
				s.start();
				runnable.run();
				double elapsedMs = ((double)s.getNanoTime()) / 1000000.d;
				AlqBasePlugin.baseinstance.log("DEBUG [Thread" + Thread.currentThread().getId() + "] " + formatToTwoDecimalPlaces(elapsedMs) + "ms " + actionName, false);
			}
			else
				runnable.run();
		});
		t.setName(actionName);
		t.start();
	}
	
	public static int getCurrentTimeInServerTicks()
	{
		// Some info:
		// 0:00AM = 18000
		// +24h = +24k ticks = 42000
		Calendar cal	= Calendar.getInstance();
		float hours		= cal.get(Calendar.HOUR_OF_DAY);
		float minutes	= cal.get(Calendar.MINUTE) + hours*60;
		float seconds	= cal.get(Calendar.SECOND) + minutes*60;
		
		float secondsInDay = 60*60*24;
		float timeOfDay = seconds / secondsInDay;
		
		// -6000=midnight; 6000=midday; 18000 = midnight
		// We have to forward time 6000
		float timeOffset = 18000;
		float currentTimeInTicks = (24000.f*timeOfDay) + timeOffset;
		return (int)currentTimeInTicks;
	}
	
	
	public static String joinArgs(String[] array)
	{ return joinArgs(array, 0); }
	public static String joinArgs(String[] array, int start)
	{
		String ret = "";
		if(array.length == 0)//<= start)
			return "";
		for(int i=start; i < array.length; ++i)
			ret += array[i] + " ";
		return ret.substring(0, ret.length()-1);
	}
	
	public static StopWatch startTiming()
	{
		StopWatch watch = new StopWatch();
		watch.start();
		return watch;
	}
	
	public static void timeStep(StopWatch watch, String msg)
	{
		long elapsedNano = watch.getNanoTime();
		float elapsedMs = (float)elapsedNano / 1000000.f;
		AlqBasePlugin.baseinstance.log("DEBUG [" + Thread.currentThread().getId() + "] " + formatToTwoDecimalPlaces(elapsedMs) + "ms " + msg, false);
		watch.reset();
		watch.start();
	}
	
	// Returns nearest lower value from map in comparison to searchedKey. defaultValue if not found.
	public static <K extends Comparable<K>, V> V getNearestLowerValue(Map<K, V> map, final K searchedKey, final V defaultValue)
	{
		K key = getNearestLowerKey(map, searchedKey);
		if(key != null)
			return map.get(key);
		return defaultValue;
	}
	
	
	// Returns nearest lower key from map in comparison to searchedKey.
	public static <K extends Comparable<K>> K getNearestLowerKey(Map<K, ?> map, K searchedKey)
	{
		K maxKey = null;
		Collection<K> keys = map.keySet();
		for(K key : keys)
			if(searchedKey.compareTo(key) >= 0)
    			if(maxKey == null) // If not set yet
    				maxKey = key;
    			else if(key.compareTo(maxKey) >= 0)
    				maxKey = key;
		return maxKey;
	}
	
	
	
	public static <V> V get(Map<?, V> map, Object key, V defaultValue)
	{
		V value = map.get(key);
		if(value == null)
			return defaultValue;
		return value;
	}
	
	
	public static <T> T convert(Object o, Class<T> targetClass)
	{ return convert(o, targetClass, null); }
	@SuppressWarnings("unchecked")
    public static <T> T convert(Object o, Class<T> targetClass, T defaultValue)
	{
		try
        {
			String s = o.toString();
			if(targetClass.equals(String.class))
				return (T)s;
			Method valueOf = targetClass.getMethod("valueOf", String.class); 
	        return (T)valueOf.invoke(null, s);
        }
        catch(IllegalAccessException | NoSuchMethodException | SecurityException e)
        {
	        e.printStackTrace();
        }
        catch(IllegalArgumentException | InvocationTargetException e) { }
		return defaultValue;
	}
}
